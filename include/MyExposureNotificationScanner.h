#ifndef _MY_EXPOSURE_NOTIFICATION_SCANNER_H
#define _MY_EXPOSURE_NOTIFICATION_SCANNER_H

#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEScan.h>
#include <BLEAdvertisedDevice.h>

#define CWA_UUID 0xfd6f
#define CWA_IDENTIFIER_LENGTH 16
#define CWA_METADATA_LENGTH 4

typedef struct {
        int rssi;
        std::string identifier;
        std::string identifierHex;
        std::string metadata;
        std::string metadataHex;
} ExposureNotification;

class MyExposureNotificationScanner {

private: // private fixed configuration
const uint16_t serviceUUID =  CWA_UUID;
const size_t serviceIdentifierLength = CWA_IDENTIFIER_LENGTH;
const size_t serviceMetadataLength = CWA_METADATA_LENGTH;
const size_t servicePayloadLength = CWA_IDENTIFIER_LENGTH + CWA_METADATA_LENGTH;

private: // private runtime variables
BLEScan* pBLEScan;
BLEScanResults scanResults;
int notificationsCount;
ExposureNotification strongestNotification;

public: // public methods
MyExposureNotificationScanner();
~MyExposureNotificationScanner();
void init();
void scan(int duration = 1);
int getNotificationsCount();
ExposureNotification getStrongestNotification();

private: // private methods
void findExposureNotifications();
std::string buildHex(std::string src);
};

#endif

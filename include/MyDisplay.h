#ifndef _MY_DISPLAY_H
#define _MY_DISPLAY_H

#include <TFT_eSPI.h>
#include <Arduino.h>

class MyDisplay {
private:
static const int width;
static const int height;
static TFT_eSPI tft;
static TFT_eSprite img;
static bool useSprite;
static TFT_eSPI* drawingObj;
public:
static void setup(bool useSprite = false);
static void clear();
static void pushSprite();
static void setCursor(int x, int y);
static void print(String msg, int fontSize = 3);
static void println(String msg, int fontSize = 3);
static void drawRectangle(int x, int y, int w, int h, bool filled = false);
};

#endif

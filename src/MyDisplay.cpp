#ifdef USE_TFT_DISPLAY
#include "MyDisplay.h"

const int MyDisplay::width = TFT_WIDTH;
const int MyDisplay::height = TFT_HEIGHT;

TFT_eSPI MyDisplay::tft = TFT_eSPI();
TFT_eSprite MyDisplay::img = TFT_eSprite(&MyDisplay::tft);
bool MyDisplay::useSprite = false;
TFT_eSPI* MyDisplay::drawingObj = &MyDisplay::tft;

void MyDisplay::setup(bool useSprite) {
        MyDisplay::useSprite = useSprite;
        MyDisplay::tft.begin();
        MyDisplay::tft.setRotation(1);
        MyDisplay::tft.setTextColor(TFT_WHITE, TFT_BLACK);
        MyDisplay::tft.fillScreen(TFT_BLACK);
        MyDisplay::tft.setTextFont(0);

        if (MyDisplay::useSprite) {
                MyDisplay::img.createSprite(MyDisplay::height, MyDisplay::width);
                MyDisplay::img.fillSprite(TFT_BLACK);
                MyDisplay::drawingObj = &MyDisplay::img;
        }
}

void MyDisplay::clear() {
        if (MyDisplay::useSprite) {
                MyDisplay::img.fillSprite(TFT_BLACK);

        } else {
                MyDisplay::tft.fillScreen(TFT_BLACK);
        }
        MyDisplay::setCursor(0,0);
}

void MyDisplay::pushSprite() {
        MyDisplay::img.pushSprite(0, 0);
}

void MyDisplay::setCursor(int x, int y) {
        if (MyDisplay::useSprite) {
                MyDisplay::img.setCursor(x, y);
        } else {
                MyDisplay::tft.setCursor(x, y);
        }
}

void MyDisplay::print(String msg, int fontSize) {
        MyDisplay::drawingObj->setTextSize(fontSize);
        MyDisplay::drawingObj->print(msg);
}

void MyDisplay::println(String msg, int fontSize) {
        MyDisplay::drawingObj->setTextSize(fontSize);
        MyDisplay::drawingObj->println(msg);

}

void MyDisplay::drawRectangle(int x, int y, int w, int h, bool filled) {
        if (filled) {
                MyDisplay::drawingObj->fillRect(x,y,w,h, TFT_WHITE);
        } else {
                MyDisplay::drawingObj->drawFastVLine(x, y, h, TFT_WHITE);
                MyDisplay::drawingObj->drawFastVLine(x+w-1, y, h, TFT_WHITE);
                MyDisplay::drawingObj->drawFastHLine(x, y, w, TFT_WHITE);
                MyDisplay::drawingObj->drawFastHLine(x, y+h-1, w, TFT_WHITE);
        }
}

#endif

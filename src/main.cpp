#include <Arduino.h>

#ifdef USE_TFT_DISPLAY
#include "MyDisplay.h"
#endif

#include "MyExposureNotificationScanner.h"

const int RSSI_MIN = -90;
const int RSSI_MAX = -40;
const int RSSI_THRESHOLD = -50;

MyExposureNotificationScanner NotificationScanner;

void setup() {
        Serial.begin(115200);

        #ifdef USE_TFT_DISPLAY
        MyDisplay::setup(true);
        #endif

        NotificationScanner.init();

        Serial.println("Exposure Notification Scanner started.");
        Serial.println("Place your device close to the scanner...");
}

void loop() {
        static std::string lastIdentifier = "";

        NotificationScanner.scan();
        ExposureNotification notification = NotificationScanner.getStrongestNotification();
        int notificationsCount = NotificationScanner.getNotificationsCount();

        Serial.print("Signals: ");
        Serial.print(String(notificationsCount));
        Serial.print(" / Strongest RSSI: ");
        Serial.println(String(notification.rssi));

        if (notification.rssi > RSSI_THRESHOLD) {
                if (lastIdentifier.compare(notification.identifier)) {
                        lastIdentifier = notification.identifier;

                        Serial.println("Strong Exposure Notification Signal detected!");
                        Serial.print("Id: ");
                        Serial.println(notification.identifierHex.c_str());
                        Serial.print("Metadata: ");
                        Serial.println(notification.metadataHex.c_str());

                        #ifdef USE_TFT_DISPLAY
                        MyDisplay::clear();
                        MyDisplay::println("Strong Signal", 3);
                        MyDisplay::println("", 2);
                        MyDisplay::print("Id: ", 2);
                        MyDisplay::println(notification.identifierHex.c_str(), 2);
                        MyDisplay::println("", 2);
                        MyDisplay::print("Data: ", 2);
                        MyDisplay::println(notification.metadataHex.c_str(), 2);
                        #endif
                }
        } else {
                lastIdentifier = "";

                #ifdef USE_TFT_DISPLAY
                MyDisplay::clear();
                MyDisplay::print("Signals: ", 3);
                MyDisplay::println(String(notificationsCount), 3);
                MyDisplay::println("", 2);
                MyDisplay::println(
                        "Try to move your\n"
                        "device closer, to\n"
                        "detect beacons of\n"
                        "Corona Warn App.", 2);
                #endif
        }

        #ifdef USE_TFT_DISPLAY
        // draw RSSI indicator
        MyDisplay::drawRectangle(0, TFT_WIDTH-11, TFT_HEIGHT, 10);
        MyDisplay::drawRectangle(0, TFT_WIDTH-11,
                                 (RSSI_THRESHOLD - RSSI_MIN) * TFT_HEIGHT / (RSSI_MAX - RSSI_MIN),
                                 10);
        if (notificationsCount) {
                MyDisplay::drawRectangle(0, TFT_WIDTH-11,
                                         (min(RSSI_MAX, notification.rssi) - RSSI_MIN)
                                         * TFT_HEIGHT / (RSSI_MAX - RSSI_MIN),
                                         10, true);
        }

        MyDisplay::pushSprite();
        #endif
}

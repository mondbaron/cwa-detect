#include "MyExposureNotificationScanner.h"

MyExposureNotificationScanner::MyExposureNotificationScanner() {
        this->pBLEScan = NULL;
        this->notificationsCount = 0;
        this->strongestNotification = {INT_MIN,"","","",""};
}
MyExposureNotificationScanner::~MyExposureNotificationScanner() {
}

void MyExposureNotificationScanner::init() {
        BLEDevice::init("");
        this->pBLEScan = BLEDevice::getScan();
}

void MyExposureNotificationScanner::scan(int duration) {
        this->scanResults = pBLEScan->start(duration, false);
        this->findExposureNotifications();
        this->pBLEScan->clearResults();
}

int MyExposureNotificationScanner::getNotificationsCount() {
        return this->notificationsCount;
}

ExposureNotification MyExposureNotificationScanner::getStrongestNotification() {
        return this->strongestNotification;
}

void MyExposureNotificationScanner::findExposureNotifications() {
        this->notificationsCount = 0;
        this->strongestNotification.rssi = INT_MIN;
        this->strongestNotification.identifier = "";
        this->strongestNotification.identifierHex = "";
        this->strongestNotification.metadata = "";
        this->strongestNotification.metadataHex = "";

        for (int n=0; n < this->scanResults.getCount(); n++) {
                BLEAdvertisedDevice device = this->scanResults.getDevice(n);
                if (device.isAdvertisingService(BLEUUID(this->serviceUUID))
                    && device.haveServiceData() && device.getServiceData().length()==this->servicePayloadLength) {
                        this->notificationsCount++;
                        int notificationRSSI = device.getRSSI();
                        std::string notificationData = device.getServiceData();
                        if (notificationRSSI > this->strongestNotification.rssi) {
                                this->strongestNotification.rssi = notificationRSSI;
                                this->strongestNotification.identifier = notificationData.substr(0, this->serviceIdentifierLength);
                                this->strongestNotification.metadata = notificationData.substr(this->serviceIdentifierLength, this->serviceMetadataLength);
                        }
                }
        }

        if (this->notificationsCount) {
                this->strongestNotification.identifierHex = this->buildHex(this->strongestNotification.identifier);
                this->strongestNotification.metadataHex = this->buildHex(this->strongestNotification.metadata);
        }
}

std::string MyExposureNotificationScanner::buildHex(std::string src) {
        std::string hexString = "";
        char hexByte[3];
        for (size_t i=0; i < src.length(); i++) {
                if (i>0) hexString += ":";
                sprintf(hexByte, "%02x", src[i]);
                hexString += std::string(hexByte);
        }
        return hexString;
}
